<?php
include_once 'includes/db_connect.php';
include_once 'includes/register.inc.php';
include_once 'includes/functions.php';

sec_session_start();

if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}
?>

<!DOCTYPE html>
<html>
<head>
<title>Test blogger</title>

<link rel="stylesheet" href="style/loginstyle.css">
<link rel="stylesheet" href="style/font.css">



      <meta charset="UTF-8">
        <script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script>
        <link rel="stylesheet" href="styles/main.css" /> 
</head>

<body background="social.jpg">

<div class="top">
	<div class="bar" >
		<a href="#" class="logoBox logoFont">The Social Network</a>	
<!--sign in -->
<div class="user logoFont">
  <?php
    if (isset($_GET['error'])) {
        echo '<p class="error">Error Logging In!</p>';
    }
  ?>
  <div class="inputSingin">
	<form action="includes/calendar.php" 
	      method="post" 
	      name="login_form"> 
	Email: <input type="text" 
				  		  name="email" 
				    	  placeholder="Email" />
	Password: <input type="password" 
                     name="password" 
                   	 id="password" 
                     placeholder="Password" />
	 <input type="button" 
          value="Sign In" 
          class="singinButton"
          onclick="formhash(this.form, this.form.password);" /> <br>
	    </div>
	</form>
</div>
</div>

	</div>
</div>

<!--sing up form-->
<div class="registerBox">
<h2>Sign Up</h2>
	<?php
	if (!empty($error_msg)) {
            echo $error_msg;
        }
    ?>

<form method="post" name="registration_form" action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>">
	<label><b>Username </b></label><br>
    <input type='text' 
    	   name='username' 
    	   id='username' 
    	   required class="inputSingup" />
    	   <br><br>   	  

    <label><b>Email</b></label><br>
    <input type="text" 
    	   name="email" 
    	   id="email" 
    	   required class="inputSingup"/>
    	   <br><br>

    <label><b>Password</b></label><br>
    <input type="password"
           name="password" 
           id="password" 
           required class="inputSingup">
           <br><br>

    <label><b>Confirm password</b></label><br>
    <input type="password" 
           name="confirmpwd" 
		   id="confirmpwd"  
           required class="inputSingup">
           <br><br>

    <input type="button" 
		   value="Register"
		   class="singupButton" 
           onclick="return regformhash(this.form,
                                       this.form.username,
                                       this.form.email,
                                   	   this.form.password,
                                       this.form.confirmpwd);" /> 	
                                        <ul>
            Usernames may contain only digits, upper and lower case letters and underscores<br>
            Emails must have a valid email format<br>
            Passwords must be at least 6 characters long<br>
            Passwords must contain <br>
                  At least one upper case letter (A..Z)<br>
                    At least one lower case letter (a..z)<br>
                    At least one number (0..9)<br>
                </ul>
            
            <li>Your password and confirmation must match exactly<br><br>
        </ul>

</div>
</form>
	</div>
</div>
</body>
</html>